module TaskBot where

import System.IO

type Task = String

-- | Run task bot.
-- Commands:
-- /list -- show task list
-- /complete -- complete the last task
-- /exit -- stop bot
-- Any other input is considered as new task.
runBot :: IO ()
runBot = do
  -- disable buffering for stdout
  hSetBuffering stdout NoBuffering
  putStrLn "Enter your name:"
  name <- getLine
  go name []
  where
    -- Helper function to interact with user and update tasks list
    go :: String -> [Task] -> IO ()
    go name tasks = do
      putStr $ name ++ "> "
      str <- getLine -- user input
      if (str == "/exit") then
        putStrLn "Goodbye!"
      else do
        -- process input unless it is an "/exit" command
        let (output, newTasks) = processCommand str tasks
        putStrLn $ "Bot> " ++ output
        go name newTasks

-- | Process user input. Returns output string to be printed by bot and
-- updated list of tasks in a tuple.
processCommand :: String -> [Task] -> (String, [Task])
processCommand str tasks = case str of
  "/complete" -> cmdComplete tasks
  "/list" -> (cmdList tasks 1, tasks)
  _ -> addTask str tasks

-- | Command to show tasks list.
cmdList :: [Task] -> Int -> String
cmdList [] 1 = "\n" ++ (show "No tasks")
cmdList [] _ = "\n" ++ (show "End of list of tasks")
cmdList (x : xs) num = "\n" ++ (show num) ++ ") " ++ (show x) ++ (cmdList xs (num + 1))

-- | Command to complete the last task.
cmdComplete :: [Task] -> (String, [Task])
cmdComplete [] = ("No tasks to complete", [])
cmdComplete (task : rest) = ("Task completed", rest)

-- | Add new task to tasks list.
addTask :: String -> [Task] -> (String, [Task])
addTask newTask tasks = ("Task added", newTask : tasks)
